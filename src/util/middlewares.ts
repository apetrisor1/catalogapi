import express from 'express'
import path from 'path'
import bodyParser from 'body-parser'
import compression from 'compression'
import cors from 'cors'
import passport from 'passport'

const corsOptions: cors.CorsOptions = {
  allowedHeaders: ['Origin', 'X-Requested-With', 'Content-Type', 'Authorization', 'Accept', 'X-Access-Token'],
  credentials: true,
  methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
  preflightContinue: false
}

export const setupMiddlewares = (app: express.Application): void => {
  app.set('port', process.env.PORT || 3000)
  app.use(bodyParser.urlencoded({ extended: true }))
  app.use(bodyParser.json())
  app.use(compression())
  app.use(cors(corsOptions))
  /*Pre-flight requests*/
  app.options('*', cors(corsOptions))
  /*Static folder for letsencrypt*/
  app.use(express.static(path.join(__dirname, 'static')))
  console.log('Static folder is: ', path.join(__dirname, 'static'))
  app.use(passport.initialize())
  app.use(passport.session())
}
