
export const MONGODB_URI = process.env['MONGODB_URI']

if (!MONGODB_URI) {
    console.log('No mongo connection string. Set MONGODB_URI environment variable.')
    process.exit(1)
}

export const JWT_SECRET = process.env['JWT_SECRET']

if (!JWT_SECRET) {
    console.log('No JWT secret string. Set JWT_SECRET environment variable.')
    process.exit(1)
}

export const MASTER_KEY = process.env['MASTER_KEY']

if (!MASTER_KEY) {
    console.log('No master key secret string. Set MASTER_KEY environment variable.')
    process.exit(1)
}

const NODE_ENV = process.env['NODE_ENV']

if (!NODE_ENV) {
    console.log('Environment not set. Set NODE_ENV environment variable.')
    process.exit(1)
}
