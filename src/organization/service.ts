import { Document } from 'mongoose'
import { Organization } from '.'
import { Address } from '../address'
import { CourseService } from '../course'
import { ICourseRequestBody, IOrganization, IOrganizationRequestBody } from '../shared/interfaces'
import { DataAccessService } from '../shared/services/dataAccess'
import { View } from '../shared/services/view'

export class OrganizationService {
  _course: CourseService = new CourseService()
  _data: DataAccessService = new DataAccessService(Organization)

  public addDefaultCourses = async (courses: Buffer, org: IOrganization): Promise<void> => {
    const payload: Array<ICourseRequestBody> = []
    courses.toString().split('|').map((name: string) => (payload.push({ name, organization: org.id })))
    await this._course.createCourses(payload)
  }

  public addOrganization = async (body: IOrganizationRequestBody): Promise<Document> => {
    return this._data.addDocument(body)
  }

  public getOrganizationById = async (id: String): Promise<Document> => {
    return this._data.getDocumentByStringId(id)
  }

  public updateAndReturnOrganization = async (org: IOrganization, body: IOrganizationRequestBody): Promise<Document> => {
    return this._data.updateAndReturnDocument(org, body)
  }

  public populate = async (document: any): Promise<boolean> => {
    await document.populate('address').execPopulate()

    const aViewer = new View(Address)
    document.address = aViewer.getView(document.address)

    return true
  }
}
