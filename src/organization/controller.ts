import * as _ from 'lodash'
import { AddressService } from '../address'
import { CourseService } from '../course'
import { IAddressRequestBody, IOrganization, IAddress } from '../shared/interfaces'
import { Organization, OrganizationService } from '.'
import { NextFunction, Request, Response } from 'express'
import { UserService } from '../user'
import { View } from '../shared/services/view'

/*GENERAL:
  Because an address is a Document but our application flow has it sent as an object, we need to:

  Delete body.address before storing body to avoid error: Cast to ObjectId failed.
  Program tries to add the address object to the FK, which expects an ObjectId.
  Create the address first, then attach its' ID to the Organization, then delete body.address.
*/

export class OrganizationController {
  _address: AddressService = new AddressService()
  _course: CourseService = new CourseService()
  _organization: OrganizationService = new OrganizationService()
  _user: UserService = new UserService()
  viewer: View = new View(Organization)

  public addMyOrganization = async ({ body, user }: Request, res: Response, next: NextFunction): Promise<Response> => {
    let courses: Buffer
    let address: IAddress
    let org: IOrganization
    const userProvidedAddress: boolean = !_.isEmpty(body.address)

    try {
      courses = await this._course.getDefaultCourses()

      if (userProvidedAddress) {
        address = await this._address.createAddress(body.address)
        org = await this._organization.addOrganization({ ...body, address: address.id })

        await Promise.all([
          this._address.updateAddress(address, { organization: org.id }),
          this._organization.populate(org)
        ])
      } else {
        delete body.address
        org = await this._organization.addOrganization(body)
      }
      await this._user.updateAndReturnUser(user, { organization: org.id })
      await this._organization.addDefaultCourses(courses, org)

      return res.status(200).send(this.viewer.getView(org))
    } catch (err) {
      next(err)
    }
  }

  public updateMyOrganization = async ({ user, body }: Request, res: Response, next: NextFunction): Promise<Response> => {
    let organization: IOrganization = await this._organization.getOrganizationById(user.organization)
    const addressBody: IAddressRequestBody = body.address
    const userProvidedAddress: boolean = !_.isEmpty(addressBody)
    const organizationHasAddress: boolean = !!(organization.address)

    try {
      if (userProvidedAddress) {
        if (organizationHasAddress) {
          await this._address.updateAddress(organization.address, addressBody)
        } else {
            const address = await this._address.createAddress({ ...addressBody, organization: organization.id })
            organization = await this._organization.updateAndReturnOrganization(organization, { address: address.id })
          }
        }
        delete body.address
        organization = await this._organization.updateAndReturnOrganization(organization, body)

        await this._organization.populate(organization)
        return res.json(this.viewer.getView(organization))
    } catch (err) {
      next(err)
    }
  }
}
