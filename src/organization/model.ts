import { Schema, Model, model } from 'mongoose'
import { IOrganization } from '../shared/interfaces/index'

export const organizationSchema: Schema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true,
    trim: true
  },
  description: {
    type: String,
    trim: true
  },
  address: {
    type: Schema.Types.ObjectId,
    ref: 'Address'
  }
})

organizationSchema.methods.keysToShow = (): Array<string> => (['_id', 'name', 'description', 'address'])

export const Organization: Model<IOrganization> = model<IOrganization>('Organization', organizationSchema)
