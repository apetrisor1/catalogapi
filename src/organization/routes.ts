import { Router } from 'express'
import { AuthController } from '../auth'
import { OrganizationController } from '.'
import { AuthorizationController } from '../shared/authorization/controller'

export class OrganizationRoutes {
  public router: Router
  public authentication: AuthController = new AuthController()
  public authorization: AuthorizationController = new AuthorizationController()
  public organizationController: OrganizationController = new OrganizationController ()

  constructor() {
    this.router = Router()
    this.routes()
  }
  routes() {
    /*NOTE:
    Admin can add an organization if he has none, at which point it is locked onto him.
    He may update it but he can not add further org's or remove his own.
    Any other regular user created belongs to that organization. This applies to admins
    as well as teachers creating other regular users.*/
    this.router.post('/',
      this.authentication.token,
      this.authorization.isAdmin,
      this.authorization.userMustNotHaveOrganization,
      this.organizationController.addMyOrganization
    )

    this.router.put('/',
      this.authentication.token,
      this.authorization.teacherOrHigher,
      this.authorization.userMustHaveOrganization,
      this.organizationController.updateMyOrganization
    )
  }
}
