/* One group === o clasa, de ex. clasa a IX-a A */
import { Group } from './model'
import { GroupController } from './controller'
import { GroupRoutes } from './routes'
import { GroupService } from './service'

export {
  Group,
  GroupController,
  GroupRoutes,
  GroupService
}
