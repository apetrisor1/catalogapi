import * as _ from 'lodash'
import { ENTITIES } from '../CONSTANTS'
import { GenericErrorBuilder } from '../shared/errors'
import { Group, GroupService } from '.'
import { IGroupRequestBodyWithDocument, IGroupRequestBody, IGroup } from '../shared/interfaces'
import { NextFunction, Request, Response } from 'express'
import { tView } from '../shared/types'
import { UserService } from '../user'
import { View } from '../shared/services/view'

export class GroupController {
  _group: GroupService = new GroupService()
  _user: UserService = new UserService()
  errorBuilder: GenericErrorBuilder = new GenericErrorBuilder()
  viewer: View = new View(Group)

  public addGroup = async ({ body, user }: Request, res: Response, next: NextFunction): Promise<Response> => {
    try {
      const { name, description } = body
      const { organization } = user
      const groupBody: IGroupRequestBody = { name, description, organization }
      const group: IGroup = await this._group.addGroup(groupBody)

      return res.status(200).send(this.viewer.getView(group))
    } catch (err) {
      next(err)
    }
  }

  public getAllGroupsInMyOrganization = async ({ user }: Request, res: Response, next: NextFunction): Promise<Response> => {
    try {
      const { organization } = user
      const rawGroups = await this._group.getGroups({ organization })
      // Avoid using map() over async calls
      for (let i = 0; i < rawGroups.length; i++) {
        await this._group.populate(rawGroups[i])
      }
      const groups: Array<tView> = rawGroups.map((group) => this.viewer.getView(group))

      return res.status(200).send(groups)
    } catch (err) {
      next(err)
    }
  }

  public updateGroupById = async (req: Request, res: Response, next: NextFunction): Promise<Response> => {
    /* Group to update is checked for existence and set in authorization middleware */
    const body: IGroupRequestBodyWithDocument = req.body
    const { name, description } = body
    const updateBody: IGroupRequestBody = { name: name || '', description: description || '' }

    try {
      let target = body.middlewareDocument
      target = await this._group.updateAndReturnGroup(target, updateBody)
      await this._group.populate(target)

      return res.status(200).send(this.viewer.getView(target))
    } catch (err) {
      next(err)
    }
  }

  public requestDoesNotHaveId = (req: Request, res: Response, next: NextFunction): void => {
    next(this.errorBuilder.requestMissingId(ENTITIES.GROUP))
  }
}
