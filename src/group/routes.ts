import { Router } from 'express'
import { AuthController } from '../auth'
import { AuthorizationController } from '../shared/authorization/controller'
import { GroupController } from '.'

export class GroupRoutes {
  public router: Router
  public authentication: AuthController = new AuthController()
  public authorization: AuthorizationController = new AuthorizationController()
  public groupController: GroupController = new GroupController ()

  constructor() {
    this.router = Router()
    this.routes()
  }
  routes() {
    this.router.post('/',
      this.authentication.token,
      this.authorization.teacherOrHigher,
      this.authorization.userMustHaveOrganization,
      this.groupController.addGroup
    )

    this.router.get('/',
    this.authentication.token,
    this.authorization.teacherOrHigher,
    this.authorization.userMustHaveOrganization,
    this.groupController.getAllGroupsInMyOrganization
    )

    this.router.put('/:groupId',
      this.authentication.token,
      this.authorization.teacherOrHigher,
      this.authorization.userMustHaveOrganization,
      this.authorization.userSharesOrganizationWithGroup,
      this.groupController.updateGroupById
    )

    this.router.put('/',
      this.authentication.token,
      this.authorization.teacherOrHigher,
      this.authorization.userMustHaveOrganization,
      this.groupController.requestDoesNotHaveId
    )
  }
}
