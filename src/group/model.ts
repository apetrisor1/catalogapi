import { Schema, Model, model } from 'mongoose'
import { IGroup } from '../shared/interfaces'

export const groupSchema: Schema = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  headteacher: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    default: undefined
  },
  organization: {
    type: Schema.Types.ObjectId,
    ref: 'Organization'
  }
})

groupSchema.methods.keysToShow = (): Array<string> => ([ '_id', 'name', 'description', 'headteacher' ])

export const Group: Model<IGroup> = model<IGroup>('Group', groupSchema)
