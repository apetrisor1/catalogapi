import { DataAccessService } from '../shared/services/dataAccess'
import { Document } from 'mongoose'
import { ENTITIES } from '../CONSTANTS'
import { GenericErrorBuilder } from '../shared/errors'
import { Group } from '.'
import { IGroup, IGroupRequestBody } from '../shared/interfaces'
import { User } from '../user'
import { View } from '../shared/services/view'

export class GroupService {
  data: DataAccessService = new DataAccessService(Group)
  errorBuilder: GenericErrorBuilder = new GenericErrorBuilder()

  public addGroup = async (body: IGroupRequestBody): Promise<Document> => {
    return this.data.addDocument(body)
  }

  public getGroupById = async (id: String): Promise<Document> => {
    return this.data.getDocumentByStringId(id)
  }

  public getGroups = async (query: Object): Promise<Document[]> => {
    return this.data.getDocuments(query)
  }

  public updateAndReturnGroup = async (group: IGroup, body: IGroupRequestBody): Promise<Document> => {
    return this.data.updateAndReturnDocument(group, body)
  }

  public checkGroupExistsAndReturnIt = async (groupId: String): Promise<Document> => {
    if (!groupId) {
      throw this.errorBuilder.requestMissingEntityId(ENTITIES.STUDENT, ENTITIES.GROUP)
    } else {
      const group: IGroup = await this.getGroupById(groupId)
      if (!group) {
        throw this.errorBuilder.custom404(ENTITIES.GROUP)
      }

      return group
    }
  }

  public populate = async (document: any): Promise<boolean> => {
    await document.populate('headteacher').execPopulate()

    const uViewer = new View(User)
    document.headteacher = uViewer.getView(document.headteacher)

    return true
  }
}
