import * as _ from 'lodash'
import { NextFunction, Request, Response } from 'express'
import { AddressService } from '../address'
import { AUTH, ENTITIES, EXCEPTIONS } from '../CONSTANTS'
import { GenericErrorBuilder, AuthErrorBuilder } from '../shared/errors'
import { GroupService } from '../group'
import { IAddressRequestBody, IAddress, IUser, IGroup } from '../shared/interfaces'
import { User, UserService } from '.'
import { USER_ROLES } from '../CONSTANTS'
import { View } from '../shared/services/view'

/*GENERAL:
  Because an address is a Document but our application flow has it sent as an object, we need to:

  Delete body.address before storing body to avoid error: Cast to ObjectId failed.
  Program tries to add the address object to the FK, which expects an ObjectId.
  Create the address first, then attach its' ID to the User, then delete body.address.
*/

export class UserController {
  _address: AddressService = new AddressService()
  _group: GroupService = new GroupService()
  _user: UserService = new UserService()
  authErrorBuilder: AuthErrorBuilder = new AuthErrorBuilder()
  errorBuilder: GenericErrorBuilder = new GenericErrorBuilder()
  viewer: View = new View(User)

  public createUser = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { body, user } = req
    let address: IAddress
    let addedUser: IUser
    let group: IGroup
    const addingAdmin: boolean = (body.role) === USER_ROLES.ADMIN
    const addingParent: boolean = (body.role) === USER_ROLES.PARENT
    const addingStudent: boolean = (body.role) === USER_ROLES.STUDENT
    const addingTeacher: boolean = (body.role) === USER_ROLES.TEACHER
    const addingHeadteacher: boolean = (addingTeacher && body.isHeadteacher)
    const providedAddress: boolean = !_.isEmpty(body.address)

    try {
      if (addingAdmin) {
        throw this.authErrorBuilder.buildUnauthorizedError(AUTH.POST, EXCEPTIONS.ADD_ADMIN_NOT_ALLOWED)
      }

      if (addingParent) {
        await this._user.checkStudentsExistInUsersOrg(body.children, user) /*Here we can check against the creator's organization.*/
      } else {
        delete body.children
      }

      if (addingStudent || addingHeadteacher) {
        group = await this._group.checkGroupExistsAndReturnIt(body.group)
      } else {
        delete body.group
      }

      /* Optional but applies to all roles. */
      if (providedAddress) {
        address = await this._address.createAddress(body.address)
        addedUser = await this._user.createUser({ ...body, address: address.id })
        await this._address.updateAddress(address, { user: addedUser.id })
      } else {
        delete body.address
        addedUser = await this._user.createUser(body)
      }

      if (addingHeadteacher) {
        await this._group.updateAndReturnGroup(group, { headteacher: addedUser.id })
      }

      /*Auto-assign organization*/
      addedUser = await this._user.updateAndReturnUser(addedUser, { organization: user.organization })
      await this._user.populate(addedUser)

      res.status(200).send(this.viewer.getView(addedUser))
    } catch (err) {
      next(err)
    }
  }

  public getMyUser = async ({ user }: Request, res: Response): Promise<Response> => {
    await this._user.populate(user)
    return res.json(this.viewer.getView(user))
  }

  public updateMe = async (req: Request, res: Response, next: NextFunction): Promise<Response> => {
    const { body } = req
    let user: IUser = req.user
    const updatingStudent = user.role === USER_ROLES.STUDENT

    if (updatingStudent) {
      delete body.group
    }
    delete body.organization
    delete body.password
    delete body.role

    try {
      user = await this.updateUser(req, user)
      await this._user.populate(user)
      return res.json(this.viewer.getView(user))
    } catch (err) {
      next(err)
    }
  }

  public updateUserById = async (req: Request, res: Response, next: NextFunction): Promise<Response> => {
    const { body } = req
    delete body.organization
    delete body.password
    delete body.role

    try {
      let user: IUser = body.middlewareDocument

      user = await this.updateUser(req, user)
      await this._user.populate(user)
      return res.json(this.viewer.getView(user))
    } catch (err) {
      next(err)
    }
  }

  private updateUser = async (req: Request, user: IUser): Promise<IUser> => {
    const { body } = req

    delete body.organization
    delete body.password
    delete body.role

    let group: IGroup
    let updatedUser: IUser
    const addressBody: IAddressRequestBody = body.address
    const providedAddress: boolean = !_.isEmpty(addressBody)
    const updatingParent: boolean = user.role === USER_ROLES.PARENT
    const updatingStudent: boolean = user.role === USER_ROLES.STUDENT
    const updatingTeacher: boolean = user.role === USER_ROLES.TEACHER
    const updatingUserToHeadteacher: boolean = (updatingTeacher && body.isHeadteacher)
    const userIsCurrentlyHeadteacher: boolean = !!(user.isHeadteacher && user.group)
    const userHasAddress: boolean = !!(user.address)
    const switchingGroups = (user: IUser, group: IGroup): boolean => (user.group.toString() !== group.id.toString())

    if (updatingStudent || updatingUserToHeadteacher) {
      group = await this._group.checkGroupExistsAndReturnIt(body.group)
    } else {
      delete body.group
    }

    if (updatingTeacher) {
      if (userIsCurrentlyHeadteacher) {

        if (updatingUserToHeadteacher) {
          switchingGroups(user, group) ?
            await Promise.all([
              this._group.updateAndReturnGroup(group, { headteacher: user.id }),
              this._group.updateAndReturnGroup(user.group, { headteacher: undefined })
            ]) :
            false
        } else {
          await Promise.all([
            this._user.updateAndReturnUser(user, { group: undefined, isHeadteacher: false }),
            this._group.updateAndReturnGroup(user.group, { headteacher: undefined })
          ])
        }

      } else {
        if (updatingUserToHeadteacher) {
          await this._group.updateAndReturnGroup(group, { headteacher: user.id })
        }
      }
    }

    if (updatingParent) {
      /* At some point, an additional check needs to be done here,
      that requesting parent is authorized to add to his children. */
      await this._user.checkStudentsExistInUsersOrg(body.children, user)
    } else {
      delete body.children
    }

    if (providedAddress) {
      if (userHasAddress) {
        await this._address.updateAddress(user.address, addressBody)
      } else {
        const address = await this._address.createAddress({ ...addressBody, user: user.id })
        updatedUser = await this._user.updateAndReturnUser(user, { address: address.id })
      }
    }
    delete body.address
    updatedUser = await this._user.updateAndReturnUser(user, body)
    await this._user.populate(updatedUser)

    return updatedUser
  }

  public requestDoesNotHaveId = (req: Request, res: Response, next: NextFunction): void => {
    next(this.errorBuilder.requestMissingId(ENTITIES.USER))
  }
}
