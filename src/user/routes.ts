import { Router } from 'express'
import { AuthController } from '../auth'
import { AuthorizationController } from '../shared/authorization/controller'
import { UserController } from '.'

export class UserRoutes {
  public router: Router
  public authentication: AuthController = new AuthController()
  public authorization: AuthorizationController = new AuthorizationController()
  public userController: UserController = new UserController()

  constructor() {
    this.router = Router()
    this.routes()
  }
  routes() {
    this.router.post('/',
      this.authentication.token,
      this.authorization.teacherOrHigher,
      this.authorization.userMustHaveOrganization,
      this.userController.createUser
    )
    this.router.get('/me',
      this.authentication.token,
      this.userController.getMyUser
    )
    this.router.put('/me',
      this.authentication.token,
      // when done testing should probably not let student update any of his info?
      this.userController.updateMe
    )
    this.router.put('/:userId',
      this.authentication.token,
      this.authorization.teacherOrHigher,
      this.authorization.userMustHaveOrganization,
      this.authorization.usersShareOrganization,
      this.userController.updateUserById
    )
    this.router.put('/',
    this.authentication.token,
    this.authorization.teacherOrHigher,
    this.userController.requestDoesNotHaveId
    )
  }
}
