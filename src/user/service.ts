import { Document } from 'mongoose'
import { User } from '.'
import { Address } from '../address'
import { Organization, OrganizationService } from '../organization/index'
import { IUserRequestBody, IUser } from '../shared/interfaces'
import { DataAccessService } from '../shared/services/dataAccess'
import { View } from '../shared/services/view'
import { AUTH, ENTITIES, EXCEPTIONS } from '../CONSTANTS'
import { AuthErrorBuilder, GenericErrorBuilder } from '../shared/errors'
import { USER_ROLES } from '../CONSTANTS'

export class UserService {
  _data: DataAccessService = new DataAccessService(User)
  _organization: OrganizationService = new OrganizationService()
  document: Document
  errorBuilder: GenericErrorBuilder = new GenericErrorBuilder()
  authError: AuthErrorBuilder = new AuthErrorBuilder()

  public checkStudentsExistInUsersOrg = async (studentIds: Array<Document>, user: IUser): Promise<void> => {
    const providedStudents: boolean = !!(studentIds && studentIds.length)
    const userHasOrganization: boolean = !!(user.organization)

    if (!userHasOrganization) {
      throw this.authError.buildUnauthorizedError(AUTH.POST, EXCEPTIONS.USER_LACKS_ORGANIZATION)
    }

    if (!providedStudents) {
      throw this.errorBuilder.requestMissingEntityId(ENTITIES.PARENT, ENTITIES.STUDENT)
    } else {
      const promises = []
      for (let i = 0; i < studentIds.length; i++) { // Avoid using map() over async calls, use regular for instead.
        promises.push(this.getUsers({ _id: studentIds[i], role: USER_ROLES.STUDENT, organization: user.organization }))
      }
      const foundStudents: Array<Array<IUser>> = (await Promise.all(promises)).filter((userArray) => userArray.length) // Other way around is fine. Filter nulls.
      console.log(foundStudents)
      const allStudentsFound = foundStudents.length === studentIds.length

      if (!allStudentsFound) {
        throw this.errorBuilder.custom404(ENTITIES.STUDENT)
      }
    }
  }

  public createUser = async (body: IUserRequestBody): Promise<Document> => {
    return this._data.addDocument(body)
  }

  public getUserById = async (id: String): Promise<Document> => {
    return this._data.getDocumentByStringId(id)
  }

  public getUsers = async (query: Object): Promise<Array<Document>> => {
    return this._data.getDocuments(query)
  }

  public updateAndReturnUser = async (user: IUser, body: IUserRequestBody): Promise<Document> => {
    return this._data.updateAndReturnDocument(user, body)
  }

  public populate = async (document: any): Promise<boolean> => {
    /*!
      Always await promise.all inside an async try catch.
      A fail in promise.all array throws reject, which is not caught in a try catch unless.
      we await the promise or write a catch block on the promise itself.
    */
    await Promise.all([
      document.populate('address').execPopulate(),
      document.populate('organization').execPopulate()
    ])

    const aViewer = new View(Address)
    const oViewer = new View(Organization)
    document.address = aViewer.getView(document.address)
    document.organization = oViewer.getView(document.organization)

    /* Populates organization.address */
    if (document.organization) {
      await this._organization.populate(document.organization)
    }

    return true
  }
}
