import bcrypt from 'bcryptjs'
import { Schema, Model, model, Error } from 'mongoose'
import { IUser } from '../shared/interfaces'
import { USER_ROLES } from '../CONSTANTS'

const roles = Array.from(Object.values(USER_ROLES))

export const userSchema: Schema = new Schema({
  username: {
    type: String,
    required: true,
    unique: true,
    trim: true
  },
  password: {
    type: String,
    required: true,
    minlength: 6
  },
  firstName: {
    type: String
  },
  middleName: {
    type: String
  },
  lastName: {
    type: String
  },
  role: {
    type: String,
    enum: roles,
    default: USER_ROLES.ADMIN
  },
  /*Student and teacher only. If teacher,
  refers to the group to which this user
  is a headteacher. (diriginte)*/
  group: {
    type: Schema.Types.ObjectId,
    ref: 'Group'
  },
  isHeadteacher: {
    type: Boolean,
    default: false
  },
  children: [ /* Parent only */
    {
      type: Schema.Types.ObjectId,
      ref: 'User'
    }
  ],
  address: {
    type: Schema.Types.ObjectId,
    ref: 'Address'
  },
  organization: {
    type: Schema.Types.ObjectId,
    ref: 'Organization'
  }
})

userSchema.pre<IUser>('save', function save(next) {
  const user = this
  user.username = user.username.toLowerCase()

  return bcrypt.genSalt(10, (err, salt) => {
    if (err) (next(err))

    bcrypt.hash(this.password, salt).then(
      (hash) => {
        user.password = hash
        next()
      }
    ).catch(err => next(err))
  })
})

userSchema.methods.comparePassword = function (candidatePassword: string, callback: any) {
  bcrypt.compare(candidatePassword, this.password, (err: Error, isMatch: boolean) => {
    return callback(err, isMatch)
  })
}

userSchema.methods.keysToShow = (): Array<string> => ([
  '_id',
  'username',
  'firstName',
  'middleName',
  'lastName',
  'role',
  'group',
  'isHeadteacher',
  'children',
  'address',
  'organization'
])

export const User: Model<IUser> = model<IUser>('User', userSchema)
