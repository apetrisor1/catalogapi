import { ApiError, ApiErrorBuilder } from '.'
import { EXCEPTIONS } from '../../CONSTANTS'

export class AuthErrorBuilder extends ApiErrorBuilder {
  public buildUnauthorizedError = (subtype: String, info: String, code = 401) => {
    return new ApiError('Unauthorized', subtype, code, info)
  }
}
