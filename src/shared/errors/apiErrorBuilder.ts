import { ERRORS } from '../../CONSTANTS'

export abstract class ApiErrorBuilder {
  public isMongoError = (err: Error): Boolean => (err.name.includes(ERRORS.MONGO))
  public isENOENT = (err: any): Boolean => (err.code === 'ENOENT')
  public isMongoDuplicateEntryError = (err: any): Boolean => (this.isMongoError(err) && err.code === 11000)
  public isValidationError = (err: Error): Boolean => (err.name.includes(ERRORS.VALIDATION))
  public isCastToObjectIDFailedError = (err: Error): Boolean => (err.message.includes(ERRORS.CAST_ID_ERROR))
}
