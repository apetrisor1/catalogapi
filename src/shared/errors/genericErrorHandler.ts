import { NextFunction, Request, Response } from 'express'
import { GenericErrorBuilder } from '.'

export class GenericErrorHandler {
  errorBuilder: GenericErrorBuilder = new GenericErrorBuilder()

  public handleError = (err: any, req: Request, res: Response, next: NextFunction) => {
    err = this.errorBuilder.mapError(err)
    res.status(err.code).json(err)
  }
}
