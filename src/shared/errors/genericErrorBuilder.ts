import { ApiError, ApiErrorBuilder } from '.'
import { ERRORS, EXCEPTIONS } from '../../CONSTANTS'

export class GenericErrorBuilder extends ApiErrorBuilder {
  public requestMissingId = (subtype: String) => {
    return new ApiError(`Missing call params`, `Please provide ${subtype} ID`, 400, `Check /:${subtype.toLowerCase()}Id`)
  }

  public requestMissingEntityId = (subtype: String, missingEntity: String) => {
    return new ApiError(`Missing call params`, `Please provide ${missingEntity} ID`, 400, `POST,PUT ${subtype} needs ${missingEntity} ID`)
  }

  public custom404 = (subtype: String) => {
    return new ApiError(`Not found`, undefined, 404, `${subtype} not found.`)
  }

  public mapError = (error: any) => {
    console.log(``)
    console.log(`<Generic error builder - map error>`)

    if (this.isCastToObjectIDFailedError(error)) {
      console.log(`<Cast to object ID failed>`) /* Avoid logging this error, reveals mongo user credentials */
      return new ApiError(error.name, EXCEPTIONS.MALFORMED_ID, 400, error.message)
    } else {
      console.log(JSON.stringify(error))
      console.log(`<end>`)

      if (this.isValidationError(error)) {
        return new ApiError(error.name, error._message, 400, error.message)
      } else if (this.isMongoDuplicateEntryError(error)) {
        return new ApiError(error.name, EXCEPTIONS.DUPLICATED_KEY, 409, error.keyValue)
      } else if (this.isENOENT(error)) {
        return new ApiError(ERRORS.ENOENT, EXCEPTIONS.STATIC_FILE_ERROR, 500, undefined)
      } else if (error instanceof ApiError) {
        return error
      }
    }

    return new ApiError(error.name, undefined, 500, error.message)
  }
}