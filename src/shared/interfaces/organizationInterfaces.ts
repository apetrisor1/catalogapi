import { Document } from 'mongoose'
import { IRequestBody } from '.'

export interface IOrganization extends Document {
  name?: string
  description?: string
  address?: Document
}

export interface IOrganizationRequestBody extends IRequestBody {
  name?: IOrganization['name']
  description?: IOrganization['description']
  address?: IOrganization['address']
}

