import { Document } from 'mongoose'
import { IRequestBody } from '.'

export interface ICourse extends Document {
  active?: boolean
  announcements?: Array<Object>
  description?: string
  name?: string
  organization?: Document
  teachers?: Array<Document>
}

export interface ICourseRequestBody extends IRequestBody {
  active?: ICourse['active']
  announcements?: ICourse['announcements']
  description?: ICourse['description']
  name?: ICourse['name']
  organization?: ICourse['organization']
  teachers?: ICourse['teachers']
}

export interface ICourseRequestBodyWithDocument extends ICourseRequestBody {
  middlewareDocument?: Document
}
