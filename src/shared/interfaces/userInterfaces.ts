import { Document } from 'mongoose'
import { IRequestBody } from '.'

export interface IUser extends Document {
  username?: string
  password?: string
  name?: string
  role?: string
  group?: Document
  isHeadteacher?: boolean
  children?: Array<Document>
  address?: Document
  organization?: Document
}

export interface IUserRequestBody extends IRequestBody {
  username?: IUser['username']
  password?: IUser['password']
  name?: IUser['name']
  role?: IUser['role']
  group?: IUser['group']
  isHeadteacher?: IUser['isHeadteacher']
  children?: IUser['children']
  address?: IUser['address']
  organization?: IUser['organization']
}

export interface IUserRequestBodyWithDocument extends IUserRequestBody {
  middlewareDocument?: Document
}
