import { Document } from 'mongoose'
import { IRequestBody } from '.'

export interface IGroup extends Document {
  name?: string
  description?: string
  headteacher?: Document
  organization?: Document
}

export interface IGroupRequestBody extends IRequestBody {
  name?: IGroup['name']
  description?: IGroup['description']
  headteacher?: IGroup['headteacher']
  organization?: IGroup['organization']
}

export interface IGroupRequestBodyWithDocument extends IGroupRequestBody {
  middlewareDocument?: Document
}

