import { IAddress, IAddressRequestBody } from './addressInterfaces'
import { ICourse, ICourseRequestBody, ICourseRequestBodyWithDocument } from './courseInterfaces'
import { IGroup, IGroupRequestBody, IGroupRequestBodyWithDocument } from './groupInterfaces'
import { IUser, IUserRequestBody, IUserRequestBodyWithDocument } from './userInterfaces'
import { IOrganization, IOrganizationRequestBody } from './organizationInterfaces'

export interface IRequestBody {
  id?: string
  _id?: string
}

export {
  IAddress,
  IAddressRequestBody,
  ICourse,
  ICourseRequestBody,
  ICourseRequestBodyWithDocument,
  IGroup,
  IGroupRequestBody,
  IGroupRequestBodyWithDocument,
  IUser,
  IUserRequestBody,
  IUserRequestBodyWithDocument,
  IOrganization,
  IOrganizationRequestBody
}