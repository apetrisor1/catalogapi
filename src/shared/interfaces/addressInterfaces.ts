import { Document } from 'mongoose'
import { IRequestBody } from '.'

export interface IAddress extends Document {
  user?: Document
  organization?: Document
  city?: string
  county?: string
  country?: string
  street1?: string
  street2?: string
  zipcode?: number
  description?: string
  phone?: number
  email?: string
  office?: string
}

export interface IAddressRequestBody extends IRequestBody {
  user?: IAddress['user']
  organization?: IAddress['organization']
  city?: IAddress['city']
  county?: IAddress['county']
  country?: IAddress['country']
  street1?: IAddress['street1']
  street2?: IAddress['street2']
  zipcode?: IAddress['zipcode']
  description?: IAddress['description']
  phone?: IAddress['phone']
  email?: IAddress['email']
  office?: IAddress['office']
}