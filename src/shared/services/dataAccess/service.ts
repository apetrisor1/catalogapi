import { Model, Document } from 'mongoose'
import { IRequestBody } from '../../interfaces'

export class DataAccessService {
  model: Model<Document>
  constructor (Model: Model<Document>) {
    this.model = Model
  }

  public addDocument = async (body: IRequestBody): Promise<Document> => {
    console.log('')
    console.log(`<DataAccessService ADD one ${this.model.modelName}>\r\n${JSON.stringify(body)}`)
    /* To allow mongo schema to throw errors on built-in validators (duplicate), wait for index to build before using it. */
    await this.model.init()
    return await this.model.create(body)
  }

  public addDocuments = async (body: Array<IRequestBody>): Promise<Array<Document>> => {
    console.log('')
    console.log(`<DataAccessService ADD many ${this.model.modelName}s>\r\n${JSON.stringify(body)}`)
    /* To allow mongo schema to throw errors on built-in validators (duplicate), wait for index to build before using it. */
    await this.model.init()
    return await this.model.create(body)
  }

  public getDocumentByStringId = async (id: String): Promise<Document> => {
    console.log('')
    console.log(`<DataAccessService GET one ${this.model.modelName}:${id}>`)
    return this.model.findOne({ _id: id })
  }

  public getDocuments = async (query: IRequestBody): Promise<Document[]> => {
    console.log('')
    console.log(`<DataAccessService GET many ${this.model.modelName}s>\nQuery: ${JSON.stringify(query)}`)
    return this.model.find(query)
  }

  public getDocumentsByIds = async (ids: Array<Document>): Promise<Document[]> => {
    console.log('')
    console.log(`<DataAccessService GET many by ${this.model.modelName} ID>\r\n ${ids}`)
    return this.model.find({ _id: { $in: ids } })
  }

  public updateAndReturnDocument = async (target: Document, body: IRequestBody): Promise<Document> => {
    console.log('')
    console.log(`<DataAccessService UPDATE one ${this.model.modelName}:${target._id}>\r\n${JSON.stringify(body)}`)
    await this.model.init()
    return this.model.findOneAndUpdate({ _id: target._id }, body, { new: true })
  }

  public updateManyAndReturnDocuments = async (ids: Array<Document>, body: IRequestBody): Promise<Array<Document>> => {
    console.log('')
    console.log(`<DataAccessService UPDATE many by ${this.model.modelName} ID>\r\n${JSON.stringify(ids)}`)
    console.log(`Body: ${JSON.stringify(body)}`)
    await this.model.init()
    return this.model.updateMany({ _id: { $in: ids } }, body, { new: true })
  }
}
