import * as _ from 'lodash'
import { NextFunction, Request, Response } from 'express'
import { AUTH, ENTITIES, EXCEPTIONS } from '../../CONSTANTS'
import { AuthErrorBuilder, GenericErrorBuilder } from '../../shared/errors'
import { User, UserService } from '../../user'
import { GroupService } from '../../group'
import { Address, AddressService } from '../../address'
import { JwtService, View } from '../../shared/services/index'
import { ICourse, IGroup, IGroupRequestBodyWithDocument, IUser, IUserRequestBodyWithDocument } from '../../shared/interfaces'
import { USER_ROLES } from '../../CONSTANTS'
import { CourseService } from '../../course'

export class AuthorizationController {
  addressService: AddressService = new AddressService()
  jwtService: JwtService = new JwtService()
  userService: UserService = new UserService()
  courseService: CourseService = new CourseService()
  groupService: GroupService = new GroupService()
  authError: AuthErrorBuilder = new AuthErrorBuilder()
  errorBuilder: GenericErrorBuilder = new GenericErrorBuilder()
  userViewer: View = new View(User)
  addressViewer: View = new View(Address)


  public isAdmin = ({ user }: Request, res: Response, next: NextFunction) => {
    if (user.role === USER_ROLES.ADMIN) {
      return next()
    } else {
      const response = this.authError.buildUnauthorizedError(AUTH.JWT, EXCEPTIONS.INVALID_JWT, 403)
      return res.status(response.code).json(response)
    }
  }

  public teacherOrHigher = ({ user }: Request, res: Response, next: NextFunction) => {
    if (user.role === USER_ROLES.ADMIN || user.role === USER_ROLES.TEACHER) {
      return next()
    } else {
      const response = this.authError.buildUnauthorizedError(AUTH.JWT, EXCEPTIONS.INVALID_JWT, 403)
      return res.status(response.code).json(response)
    }
  }

  public userMustHaveOrganization = ({ user }: Request, res: Response, next: NextFunction) => {
    if (user.organization) {
      return next()
    } else {
      const err = this.authError.buildUnauthorizedError(AUTH.POST, EXCEPTIONS.USER_LACKS_ORGANIZATION, 403)
      return res.status(err.code).json(err)
    }
  }

  public userMustNotHaveOrganization = ({ user }: Request, res: Response, next: NextFunction) => {
    if (!user.organization) {
      return next()
    } else {
      const err = this.authError.buildUnauthorizedError(AUTH.POST, EXCEPTIONS.USER_HAS_ORGANIZATION, 403)
      return res.status(err.code).json(err)
    }
  }

  public userSharesOrganizationWithGroup = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { params, user } = req
      const { groupId } = params
      const body: IGroupRequestBodyWithDocument = req.body
      const group: IGroup = await this.groupService.getGroupById(groupId)

      if (!group) {
        const err = this.errorBuilder.custom404(ENTITIES.GROUP)
        return res.status(err.code).json(err)
      }
      const groupIsInAnotherOrganization = user.organization.toString() !== group.organization.toString()

      if (groupIsInAnotherOrganization) {
        const err = this.authError.buildUnauthorizedError(ENTITIES.GROUP, EXCEPTIONS.USER_NOT_AUTHORIZED_ON_ORGANIZATION, 403)
        return res.status(err.code).json(err)
      }
      // Passed further up the chain, so we don't have to query again for it.
      body.middlewareDocument = group
      return next()
    } catch (err) {
      err = this.errorBuilder.mapError(err)
      res.status(err.code).json(err)
    }
  }

  public userSharesOrganizationWithCourse = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { params, user } = req
      const { courseId } = params
      const body: IGroupRequestBodyWithDocument = req.body
      const course: ICourse = await this.courseService.getCourseById(courseId)

      if (!course) {
        const err = this.errorBuilder.custom404(ENTITIES.COURSE)
        return res.status(err.code).json(err)
      }
      const courseIsInAnotherOrganization = user.organization.toString() !== course.organization.toString()

      if (courseIsInAnotherOrganization) {
        const err = this.authError.buildUnauthorizedError(ENTITIES.COURSE, EXCEPTIONS.USER_NOT_AUTHORIZED_ON_ORGANIZATION, 403)
        return res.status(err.code).json(err)
      }
      // Passed further up the chain, so we don't have to query again for it.
      body.middlewareDocument = course
      return next()
    } catch (err) {
      err = this.errorBuilder.mapError(err)
      res.status(err.code).json(err)
    }
  }

  public usersShareOrganization = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { params, user } = req
      const { userId } = params
      const body: IUserRequestBodyWithDocument = req.body
      const targetUser: IUser = await this.userService.getUserById(userId)

      if (!targetUser) {
        throw this.errorBuilder.custom404(ENTITIES.USER)
      }
      const userDoNotShareOrg = user.organization.toString() !== targetUser.organization.toString()

      if (userDoNotShareOrg) {
        throw this.authError.buildUnauthorizedError(ENTITIES.USER, EXCEPTIONS.USER_NOT_AUTHORIZED_ON_ORGANIZATION, 403)
      }
      // Passed further up the chain, so we don't have to query again for it.
      body.middlewareDocument = targetUser
      return next()
    } catch (err) {
      err = this.errorBuilder.mapError(err)
      res.status(err.code).json(err)
    }
  }
}
