import { Router } from 'express'
import { AuthController } from '.'

export class AuthRoutes {
  public router: Router
  public authentication: AuthController = new AuthController()

  constructor() {
    this.router = Router()
    this.routes()
  }
  routes() {
    this.router.post('/register',
      this.authentication.masterKey,
      this.authentication.register
    )
    this.router.post('/login',
      this.authentication.masterKey,
      this.authentication.login
    )
  }
}
