import * as _ from 'lodash'
import passport from 'passport'
import { NextFunction, Request, Response } from 'express'
import { AUTH, EXCEPTIONS } from '../CONSTANTS'
import { AuthErrorBuilder } from '../shared/errors'
import { User, UserService } from '../user'
import { Address, AddressService } from '../address'
import { JwtService, View } from '../shared/services/index'
import { IUser, IAddress } from '../shared/interfaces'

export class AuthController {
  _address: AddressService = new AddressService()
  _jwt: JwtService = new JwtService()
  _user: UserService = new UserService()
  authError: AuthErrorBuilder = new AuthErrorBuilder()
  addressViewer: View = new View(Address)
  userViewer: View = new View(User)

  public login = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    passport.authenticate('local', async (err, user, info) => {
      if (err) return next(err)
      if (!user) {
        next(this.authError.buildUnauthorizedError(AUTH.LOGIN, info.message))
      } else {
        await this._user.populate(user)

        res.status(200).send({
          token: this._jwt.getToken({ id: user.id }),
          user: this.userViewer.getView(user)
        })
      }
    })(req, res, next)
  }

  /* Used to create admin only. */
  public register = async ({ body }: Request, res: Response, next: NextFunction): Promise<Response> => {
    const userProvidedAddress = !_.isEmpty(body.address)
    let address: IAddress
    let user: IUser

    try {
      if (userProvidedAddress) {
        address = await this._address.createAddress(body.address)
        user = await this._user.createUser({ ...body, address: address.id })

        await Promise.all([
          this._address.updateAddress(address, { user: user.id }),
          this._user.populate(user)
        ])
      } else {
        delete body.address
        user = await this._user.createUser(body)
      }

      return res.status(200).send({
        token: this._jwt.getToken({ id: user.id }),
        user: this.userViewer.getView(user)
      })
    } catch (err) {
      next(err)
    }
  }

  public masterKey = (req: Request, res: Response, next: NextFunction) => {
    passport.authenticate('masterKey', (err, user, info) => {
      if (err || !user) {
        next(this.authError.buildUnauthorizedError(AUTH.MASTER_KEY, EXCEPTIONS.INVALID_MASTER_KEY))
      } else {
        return next()
      }
    })(req, res, next)
  }

  public token = (req: Request, res: Response, next: NextFunction) => {
    passport.authenticate('token', (err, user, info) => {
      if (err) {
        next(err)
      } else if (!user) {
        next(this.authError.buildUnauthorizedError(AUTH.JWT, EXCEPTIONS.INVALID_JWT))
      } else {
        req.user = user
        return next()
      }
    })(req, res, next)
  }
}
