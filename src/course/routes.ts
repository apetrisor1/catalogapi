import { Router } from 'express'
import { AuthController } from '../auth'
import { AuthorizationController } from '../shared/authorization/controller'
import { CourseController } from '.'

export class CourseRoutes {
  public router: Router
  public authentication: AuthController = new AuthController()
  public authorization: AuthorizationController = new AuthorizationController()
  public courseController: CourseController = new CourseController ()

  constructor() {
    this.router = Router()
    this.routes()
  }
  routes() {
    this.router.get('/',
    this.authentication.token,
    this.authorization.userMustHaveOrganization,
    this.courseController.getCourses
    )

    this.router.post('/activate',
    this.authentication.token,
    this.authorization.teacherOrHigher,
    this.authorization.userMustHaveOrganization,
    this.courseController.activateCourses
    )

    this.router.post('/',
    this.authentication.token,
    this.authorization.teacherOrHigher,
    this.authorization.userMustHaveOrganization,
    this.courseController.addCourse
    )

    this.router.put('/:courseId',
    this.authentication.token,
    this.authorization.teacherOrHigher,
    this.authorization.userMustHaveOrganization,
    this.authorization.userSharesOrganizationWithCourse,
    this.courseController.updateCourseById
    )

    this.router.put('/',
    this.authentication.token,
    this.authorization.teacherOrHigher,
    this.authorization.userMustHaveOrganization,
    this.courseController.requestDoesNotHaveId
    )
  }
}
