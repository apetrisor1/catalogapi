const fs = require('fs')
import * as _ from 'lodash'
import { Course, CourseService } from '.'
import { ENTITIES } from '../CONSTANTS'
import { GenericErrorBuilder, AuthErrorBuilder } from '../shared/errors'
import { ICourse, ICourseRequestBody, ICourseRequestBodyWithDocument } from '../shared/interfaces'
import { NextFunction, Request, Response } from 'express'
import { View } from '../shared/services/view'
import { tView } from '../shared/types'

export class CourseController {
  _course: CourseService = new CourseService()
  authError: AuthErrorBuilder = new AuthErrorBuilder()
  errorBuilder: GenericErrorBuilder = new GenericErrorBuilder()
  viewer: View = new View(Course)

  public activateCourses = async ({ user, body }: Request, res: Response, next: NextFunction) => {
    try {
      const { courses: courseIds } = body
      const { organization } = user

      await this._course.checkCoursesExistOnMyOrg(courseIds, user)
      await this._course.updateCoursesByIds(courseIds, { active: true })

      const rawCourses = await this._course.getCourses({ active: true, organization })
      const courses: Array<tView> = rawCourses.map((course) => (this.viewer.getView(course)))

      return res.status(200).send(courses)
    } catch (err) {
      next(err)
    }
  }

  public addCourse = async ({ user, body }: Request, res: Response, next: NextFunction) => {
    try {
      const { name, description, active = true } = body
      const { organization } = user
      const payload: ICourseRequestBody = { name, description, active, organization }

      const addedCourse: ICourse = await this._course.createCourse(payload)
      const course: tView = this.viewer.getView(addedCourse)

      return res.status(200).send(course)
    } catch (err) {
      next(err)
    }
  }

  public updateCourseById = async (req: Request, res: Response, next: NextFunction) => {
    const body: ICourseRequestBodyWithDocument = req.body
    const { active = true, announcements, description, name, teachers } = body
    const { user } = req
    const { organization } = user

    let target: ICourse = body.middlewareDocument
    const payload: ICourseRequestBody = {
      active,
      announcements: announcements || [],
      description: description || '',
      name: name || '',
      organization,
      teachers: teachers || [] // TO DO: CHECK IF TEACHERS IN MY ORG
    }

    try {
      target = await this._course.updateCourseAndReturn(target, payload)

      return res.status(200).send(this.viewer.getView(target))
    } catch (err) {
      next(err)
    }
  }

  public getCourses = async ({ user, query }: Request, res: Response, next: NextFunction): Promise<Response> => {
    try {
      const { active = true } = query
      const { organization } = user
      const rawCourses = await this._course.getCourses({ active, organization })
      const courses: Array<tView> = rawCourses.map((course) => (this.viewer.getView(course)))

      return res.status(200).send(courses)
    } catch (err) {
      next(err)
    }
  }

  public requestDoesNotHaveId = (req: Request, res: Response, next: NextFunction): void => {
    next(this.errorBuilder.requestMissingId(ENTITIES.COURSE))
  }
}
