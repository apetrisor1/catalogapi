import { Course } from '.'
import { DataAccessService } from '../shared/services/dataAccess'
import { Document } from 'mongoose'
import { GenericErrorBuilder } from '../shared/errors'
import { ICourse, ICourseRequestBody, IUser } from '../shared/interfaces'
import { ENTITIES } from '../CONSTANTS'
const fs = require('fs')

export class CourseService {
  _data: DataAccessService = new DataAccessService(Course)
  errorBuilder: GenericErrorBuilder = new GenericErrorBuilder()

  public checkCoursesExistOnMyOrg = async (courseIds: Array<Document>, user: IUser): Promise<void> => {
    const providedCourses: boolean = !!(courseIds && courseIds.length)
    if (!providedCourses) {
      throw this.errorBuilder.requestMissingEntityId(ENTITIES.COURSE, ENTITIES.COURSE)
    } else {
      const foundCourses: Array<ICourse> = (await this.getCoursesByIds(courseIds)).filter((course) => {
        return course.organization.toString() === user.organization.toString()
      })
      const allFound = courseIds.length === foundCourses.length

      if (!allFound) {
        throw this.errorBuilder.custom404(ENTITIES.COURSE)
      }
    }
  }

  public createCourse = async (body: ICourseRequestBody): Promise<ICourse> => {
    return this._data.addDocument(body)
  }

  public createCourses = async (body: Array<ICourseRequestBody>): Promise<Array<ICourse>> => {
    return this._data.addDocuments(body)
  }

  public getCourses = async (query: ICourseRequestBody): Promise<ICourse[]> => {
    return this._data.getDocuments(query)
  }

  public getCoursesByIds = async (courseIds: Array<Document>): Promise<ICourse[]> => {
    return this._data.getDocumentsByIds(courseIds)
  }

  public getCourseById = async (id: String): Promise<Document> => {
    return this._data.getDocumentByStringId(id)
  }

  public getDefaultCourses = async (): Promise<Buffer> => (
    fs.readFileSync(process.cwd() + '/src/DEFAULT_COURSES.csv')
  )

  public updateCourseAndReturn = async (course: ICourse, body: ICourseRequestBody): Promise<ICourse> => {
    return this._data.updateAndReturnDocument(course, body)
  }

  public updateCoursesByIds = async (courseIds: Array<Document>, body: ICourseRequestBody): Promise<Array<Document>> => {
    return this._data.updateManyAndReturnDocuments(courseIds, body)
  }

  // async populate ()
}
