import { Schema, Model, model } from 'mongoose'
import { ICourse } from '../shared/interfaces'

export const courseSchema: Schema = new Schema({
  active: {
    type: Boolean,
    default: false
  },
  announcements: [
    { type: Object }
  ],
  description: {
    type: String,
    trim: true
  },
  name: {
    type: String,
    required: true,
    trim: true
  },
  organization: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'Organization'
  },
  teachers: [
    {
      type: Schema.Types.ObjectId,
      ref: 'User'
    }
  ]
})

courseSchema.methods.keysToShow = (): Array<string> => ([
  '_id',
  'active',
  'announcements',
  'description',
  'name',
  'teachers'
])

courseSchema.index({ organization: -1 })

export const Course: Model<ICourse> = model<ICourse>('Course', courseSchema)
