const AUTH = {
  JWT: 'JWT',
  LOGIN: 'Login',
  MASTER_KEY: 'masterKey',
  POST: 'POST'
}

const ENTITIES = {
  USER: 'User',
  GROUP: 'Group',
  COURSE: 'Course',
  STUDENT: 'Student',
  PARENT: 'Parent',
  CHILDREN: 'Children',
  TEACHER: 'Teacher',
  DOCUMENT: 'Document'
}

/* Received */
const ERRORS = {
  CAST_ID_ERROR: 'Cast to ObjectId',
  MONGO: 'MongoError',
  VALIDATION: 'ValidationError',
  ENOENT: 'ENOENT'
}

/* Thrown */
const EXCEPTIONS = {
  ADD_ADMIN_NOT_ALLOWED: 'Adding admin not allowed.',
  STATIC_FILE_ERROR: 'Something went wrong when trying to access a static file on this server. Please contact your administrator.',
  DUPLICATED_KEY: 'Duplicated key',
  INVALID_JWT: 'Invalid or unauthorized Bearer <JWT>',
  INVALID_MASTER_KEY: 'Invalid or missing Bearer <masterKey>',
  MALFORMED_ID: 'You might have entered a malformed ID.',
  USER_NOT_AUTHORIZED_ON_ORGANIZATION: 'You are not authorized to access (associated) organization.',
  USER_HAS_ORGANIZATION: 'You are already in an organization.',
  USER_LACKS_ORGANIZATION: 'You do not yet have an organization - POST /organization'
}

const USER_ROLES = {
  ADMIN: 'admin',
  TEACHER: 'teacher',
  PARENT: 'parent',
  STUDENT: 'student'
}

export {
  AUTH,
  ENTITIES,
  ERRORS,
  EXCEPTIONS,
  USER_ROLES
}
