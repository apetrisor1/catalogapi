import { AuthRoutes } from './auth'
import { CourseRoutes } from './course'
import { GenericErrorHandler } from './shared/errors'
import { GroupRoutes } from './group'
import { UserRoutes } from './user'
import { OrganizationRoutes } from './organization'
import express from 'express'

export const setupMainRoutes = (app: express.Application): void => {
  const errorHandler: GenericErrorHandler = new GenericErrorHandler()

  app.use('/api/auth', new AuthRoutes().router)
  app.use('/api/course', new CourseRoutes().router)
  app.use('/api/group', new GroupRoutes().router)
  app.use('/api/organization', new OrganizationRoutes().router)
  app.use('/api/user', new UserRoutes().router)
  app.use(errorHandler.handleError)
}
