import { Address } from '.'
import { DataAccessService } from '../shared/services/dataAccess'
import { Document } from 'mongoose'
import { IAddress, IAddressRequestBody } from '../shared/interfaces'

export class AddressService {
  _data: DataAccessService = new DataAccessService(Address)

  public createAddress = async (body: IAddressRequestBody): Promise<Document> => {
    return this._data.addDocument(body)
  }

  public updateAddress = async (address: IAddress, body: IAddressRequestBody): Promise<Document> => {
    return this._data.updateAndReturnDocument(address, body)
  }
}
