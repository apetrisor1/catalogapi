import { Schema, Model, model } from 'mongoose'
import { IAddress } from '../shared/interfaces'

export const addressSchema: Schema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  organization: {
    type: Schema.Types.ObjectId,
    ref: 'Organization'
  },
  city: {
    type: String
  },
  county: {
    type: String
  },
  country: {
    type: String,
  },
  street1: {
    type: String
  },
  street2: {
    type: String
  },
  zipcode: {
    type: Number
  },
  description: {
    type: String
  },
  phone: {
    type: Number
  },
  email: {
    type: String
  },
  office: {
    type: String
  }
})

addressSchema.methods.keysToShow = (): Array<string> => ([ '_id', 'city', 'county', 'country', 'street1', 'street2', 'zipcode', 'description', 'phone', 'email', 'office' ])

export const Address: Model<IAddress> = model<IAddress>('Address', addressSchema)
