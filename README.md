# Installation
- Clone the repository
```
git clone URL
```
- Install dependencies
```
cd FOLDER_NAME
npm install
npm run build
```

 - Locally, configure (.env) the cloud Mongo server and start Node server with
`npm run start-dev`

Compiling from TS to JS is done in .ebextensions/source_compile.config, between npm install and npm start
AWS then runs npm start

- NOT TESTED: Launch demo Node and Mongo server in docker containers
```
docker-compose build
docker-compose up
```

# Getting started

## Step1 : Register a user
Send a POST request to `http://localhost:3000/api/user/register` 
with the following payload ** :
```json
{
	"username": "me",
	"password": "pass"
}
```
You should get a JWT token in the response :
```json
{
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1lMiIsImlhdCI6MTU1MDU4MTA4NH0.WN5D-BFLypnuklvO3VFQ5ucDjBT68R2Yc-gj8AlkRAs"
}
```
## Step2 : Authorization w/ Passport strategies
Pass the following headers for:

Routes protected with `masterKey` **E.g. /auth/register**
```
Authorization     Bearer masterKey
```

Routes protected with `jwt` **E.g. /user/me**
```
Authorization     Bearer token
```

Add more strategies to passport or extend the `token` strategy to fine-grain authorization.
