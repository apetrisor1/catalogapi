#!/bin/bash
sudo su
cd /

if [[ -d /acme.sh ]]
then
    rm -rf /acme.sh
fi

git clone https://github.com/acmesh-official/acme.sh.git \
&& cd ./acme.sh \
&& ./acme.sh --install