#!/bin/bash
moveCerts () {
    if [[ ! -d "/$1" ]]
    then
        mkdir $1
    fi

    cd ~/.acme.sh
    cp -R ./$1 /etc/nginx/certs
}

sudo su
#Make Nginx certs folder
cd /etc/nginx
if [ ! -d "/certs" ]
then
    mkdir certs
fi
cd certs

#
if [ "$NODE_ENV" = "staging" ]
then
    moveCerts $STAGING_DOMAIN_NAME
fi

if [ "$NODE_ENV" = "production" ]
then
    moveCerts $PRODUCTION_DOMAIN_NAME
fi