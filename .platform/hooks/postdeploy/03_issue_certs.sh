#!/bin/bash
issueCerts () {
    sudo /acme.sh/acme.sh --issue --force -d "$1" -d "www.$1" -w /var/app/current/dist/util/static
}

if [ "$NODE_ENV" = "staging" ]
then
    issueCerts $STAGING_DOMAIN_NAME && echo 'Issued staging certificates'
fi

if [ "$NODE_ENV" = "production" ]
then
    issueCerts $PRODUCTION_DOMAIN_NAME && echo 'Issued production certificates'
fi
