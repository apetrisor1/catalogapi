#!/bin/bash
sudo su

cd /var/app/current/dist/util

# /var/app/current/static
if [ -d "/static" ]
then
    echo "Static folder already exists"
else
    mkdir static
    echo "Created static folder"
fi
cd static

# /var/app/current/static/.well-known
if [ -d "/.well-known" ]
then
    echo ".well-known folder already exists"
else
    mkdir .well-known
    echo "Created well-known folder"
fi
cd .well-known

# /var/app/current/static/.well-known/acme-challenge
if [ -d "/.acme-challenge" ]
then
    echo "acme-challenge folder already exists"
else
    mkdir acme-challenge
    echo "Created acme-challenge folder"
fi
cd acme-challenge

# /var/app/current/static/.well-known/acme-challenge/test.txt
if [ -a "/test.txt" ]
then
    echo "Test file already exists"
else
    echo "testing123." > test.txt
fi

# Check logs for testing123 x2
cat test.txt && cat test.txt