#!/bin/bash
removeOldCerts () {
    echo "Removing old certificates...."
    sudo rm -rf .acme.sh/$1
    echo "...removed"
}

sudo su
cd ~

if [ "$NODE_ENV" = "staging" ] && [ -d "/.acme.sh/$STAGING_DOMAIN_NAME" ]
then
    removeOldCerts $STAGING_DOMAIN_NAME
fi

if [ "$NODE_ENV" = "production" ] && [ -d "/.acme.sh/$PRODUCTION_DOMAIN_NAME" ]
then
    removeOldCerts $PRODUCTION_DOMAIN_NAME
fi
