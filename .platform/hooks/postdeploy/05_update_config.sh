#!/bin/bash
replaceNginxConfigWithCustomConfig() {
    # Use command to avoid alias using interactive mode: -i
    command cp -f /var/app/current/ssl/nginx.conf /etc/nginx/nginx.conf
}

updateConfigWithDomain () {
    sed -i "s/<URL>/$1/g" /etc/nginx/nginx.conf
}

sudo su
replaceNginxConfigWithCustomConfig

if [ "$NODE_ENV" = "staging" ]
then
    updateConfigWithDomain $STAGING_DOMAIN_NAME
fi

if [ "$NODE_ENV" = "production" ]
then
    updateConfigWithDomain $PRODUCTION_DOMAIN_NAME
fi